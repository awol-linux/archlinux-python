# Docker in Docker with Molecule

Dockerfile to build a Docker in Docker image with molecule installed for testing ansible roles.
It's based off Alpine Stabel DiND and installs:

- python3
- bash

You will need Docker set up and running on your system.

## Build

```
build . -t awollinux/archlinux-python:latest
```

If the build is successful, you'll be able to see your new image, for example:

```
[awol@gaming-amd ~]$ docker images
REPOSITORY                TAG                 IMAGE ID            CREATED              SIZE
REPOSITORY                   TAG                 IMAGE ID            CREATED             SIZE
awollinux/archlinux-python   latest              54988f9e0f01        9 minutes ago       603MB
archlinux                    latest              6df2f5075232        7 days ago          467MB
[awol@gaming-amd ~]$ 
```

## Run

```
docker run -it awollinux/dind-molecule /bin/bash
```